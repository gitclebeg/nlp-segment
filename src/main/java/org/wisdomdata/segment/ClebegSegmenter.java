package org.wisdomdata.segment;

public interface ClebegSegmenter {
	String segment(String text);
}
