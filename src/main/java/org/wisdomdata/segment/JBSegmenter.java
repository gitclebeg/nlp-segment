package org.wisdomdata.segment;

import java.util.List;




import com.huaban.analysis.jieba.JiebaSegmenter;
import com.huaban.analysis.jieba.SegToken;
import com.huaban.analysis.jieba.JiebaSegmenter.SegMode;

public class JBSegmenter implements ClebegSegmenter{
	private static JiebaSegmenter segmenter = new JiebaSegmenter();
	
	public String segment(String title) {
		List<SegToken> tokens = segmenter.process(title, SegMode.INDEX);
        StringBuilder sb = new StringBuilder() ;
        for (SegToken t : tokens) {
            sb.append(t.word.getToken()) ;
            sb.append(" ");
        }
        return sb.toString();
    }
	
}
